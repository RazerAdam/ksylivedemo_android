package com.ksyun.media.streamer.demo;

import com.ksyun.live.demo.R;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.TextView;

import com.ksyun.media.streamer.capture.ViewCapture;
import com.ksyun.media.streamer.kit.KSYStreamer;
import com.lht.paintview.PaintView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;

/**
 * Fragment for video settings.
 */

public class StreamFuncFragment extends Fragment {
    public static final String TAG = "StreamFuncFragment";
    protected static final int PAINT_VIEW_IDX = 7;

    @BindView(R.id.front_camera_mirror)
    protected CheckBox mFrontMirrorCB;
    @BindView(R.id.paint_streaming)
    protected CheckBox mPaintStreamingCB;

    protected Unbinder mUnbinder;

    protected StdCameraActivity mActivity;
    protected KSYStreamer mStreamer;
    protected ViewCapture mPaintViewCapture;
    protected ViewCapture mTextViewCapture;
    protected ViewCapture mWebViewCapture;
    protected PaintView mPaintView;
    protected TextView textView;
    protected WebView webView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stream_func_fragment, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mActivity = (StdCameraActivity) getActivity();
        mStreamer = mActivity.mStreamer;
        mPaintView = mActivity.mPaintView;
        textView = mActivity.textView;
        webView = mActivity.webView;
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePaintViewCapture();
    }

    @OnCheckedChanged(R.id.front_camera_mirror)
    protected void onFrontMirrorChecked(boolean isChecked) {
        mStreamer.setFrontCameraMirror(isChecked);
    }

    @OnCheckedChanged(R.id.paint_streaming)
    protected void onPaintStreamingChecked(boolean isChecked) {
        if (isChecked) {
            // config paint view
            mPaintView.setVisibility(View.VISIBLE);
            mPaintView.setColor(Color.RED);
            mPaintView.setBgColor(Color.TRANSPARENT);
            mPaintView.setStrokeWidth(4);
            mPaintView.setGestureEnable(false);

            initPaintViewCapture();
            initTextViewCapture();
            initWebViewCapture();

            startPaintViewCapture();
            startTextViewCapture();
            startWebViewCapture();
        } else {
            stopPaintViewCapture();
            stopTextViewCapture();
            stopWebViewCapture();
            mPaintView.clear();
            mPaintView.setVisibility(View.GONE);
        }
    }

    private void initTextViewCapture(){
        if (mTextViewCapture != null){
            return;
        }
        mTextViewCapture = new ViewCapture(mStreamer.getGLRender());
        mTextViewCapture.getSrcPin().connect(mStreamer.getImgTexMixer().getSinkPin(5));
        mStreamer.getImgTexMixer().setRenderRect(5, 0.05f, 0.09f, 0, 0.25f, 0.8f);

        textView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                int oldW = oldRight - oldLeft;
                int oldH = oldBottom - oldTop;
                if (mPaintStreamingCB.isChecked() && (oldW * oldH != 0)) {
                    stopTextViewCapture();
                    startTextViewCapture();
                }
            }
        });
    }

    protected void initPaintViewCapture() {
        if (mPaintViewCapture != null) {
            return;
        }

        mPaintViewCapture = new ViewCapture(mStreamer.getGLRender());
        // connect to the empty last sink pin of graph mixer
        mPaintViewCapture.getSrcPin().connect(mStreamer.getImgTexMixer().getSinkPin(6));
        // set render position relative to the video
        mStreamer.getImgTexMixer().setRenderRect(PAINT_VIEW_IDX, 0, 0, 1, 1, 1);

        // restart PaintViewCapture while view layout changed
        mPaintView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                int oldW = oldRight - oldLeft;
                int oldH = oldBottom - oldTop;
                if (mPaintStreamingCB.isChecked() && (oldW * oldH != 0)) {
                    stopPaintViewCapture();
                    startPaintViewCapture();
                }
            }
        });
    }

    protected void initWebViewCapture() {
        if (mWebViewCapture != null) {
            return;
        }

        mWebViewCapture = new ViewCapture(mStreamer.getGLRender());
        // connect to the empty last sink pin of graph mixer
        mWebViewCapture.getSrcPin().connect(mStreamer.getImgTexMixer().getSinkPin(5));
        // set render position relative to the video
        mStreamer.getImgTexMixer().setRenderRect(PAINT_VIEW_IDX, 0, 0, 1, 1, 1);

        // restart PaintViewCapture while view layout changed
        webView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                int oldW = oldRight - oldLeft;
                int oldH = oldBottom - oldTop;
                if (mPaintStreamingCB.isChecked() && (oldW * oldH != 0)) {
                    stopPaintViewCapture();
                    startPaintViewCapture();
                }
            }
        });
    }

    protected void startPaintViewCapture() {
        if (mPaintViewCapture != null) {
            mPaintViewCapture.setTargetResolution(mStreamer.getTargetWidth(), mStreamer.getTargetHeight());
            mPaintViewCapture.setUpdateFps(mStreamer.getTargetFps());
            mPaintViewCapture.start(mPaintView);
        }
    }

    protected void stopPaintViewCapture() {
        if (mPaintViewCapture != null) {
            mPaintViewCapture.stop();
        }
    }

    protected void releasePaintViewCapture() {
        if (mPaintViewCapture != null) {
            mPaintViewCapture.release();
        }
    }

    protected void startTextViewCapture() {
        if (mTextViewCapture != null) {
            mTextViewCapture.setTargetResolution(mStreamer.getTargetWidth(), mStreamer.getTargetHeight());
            mTextViewCapture.setUpdateFps(mStreamer.getTargetFps());
            mTextViewCapture.start(textView);
        }
    }

    protected void stopTextViewCapture() {
        if (mTextViewCapture != null) {
            mTextViewCapture.stop();
        }
    }

    protected void releaseTextViewCapture() {
        if (mTextViewCapture != null) {
            mTextViewCapture.release();
        }
    }

    protected void startWebViewCapture() {
        if (mWebViewCapture != null) {
            mWebViewCapture.setTargetResolution(mStreamer.getTargetWidth(), mStreamer.getTargetHeight());
            mWebViewCapture.setUpdateFps(mStreamer.getTargetFps());
            mWebViewCapture.start(webView);
        }
    }

    protected void stopWebViewCapture() {
        if (mWebViewCapture != null) {
            mWebViewCapture.stop();
        }
    }

    protected void releaseWebViewCapture() {
        if (mWebViewCapture != null) {
            mWebViewCapture.release();
        }
    }
}
